import urllib.parse
import requests
import sys
import codecs
from bs4 import BeautifulSoup


def displayContent(content):
    pass


def parsePageStories(page_url):
    stories = []
    page_content = getPageContent(page_url)
    soup = BeautifulSoup(page_content, 'html.parser')
    articles = soup.findAll('article')
    for article in articles:
        story_headline = article.find('h2').text.strip()
        story_teaser = article.find('div', {'class': 'teaser-text'}).text.strip()
        story_url = urllib.parse.urljoin(page_url, article.find('a')['href'])
        stories.append((story_headline, story_teaser, story_url))
    return stories


def getStory(story_url):
    story_content = getPageContent(story_url)
    soup = BeautifulSoup(story_content, 'html.parser')
    paragraph_list = soup.find('div', {'class': 'node__content'}).findAll('p')
    for paragraph in paragraph_list:
        print(paragraph.text.strip())


def displayHeadlines(stories):
    idx = 0
    for story in stories:
        story_str = "[{}] {}".format(idx+1, story[0])
        print(story_str)
        idx+= 1


def getPageContent(page_url):
    page_content = None
    user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36'
    headers = {'User-Agent': user_agent}
    r = requests.get(page_url, headers=headers, verify=False)
    if r.status_code == 200:
        page_content = r.content
    return page_content

if __name__ == '__main__':
    page_url = "https://zerohedge.com"
    stories = parsePageStories(page_url)
    displayHeadlines(stories)
    story_idx = int(input("Select story: "))
    while story_idx != 0:
        if story_idx > len(stories):
            print("Invalid story index")
        else:
            story_url = stories[story_idx-1][2]
            getStory(story_url)
            displayHeadlines(stories)
            story_idx = int(input("Select story: "))

    