# ZeroHedgeHeadlines

News headlines/articles parser from zerohedge.com.
Provides command line access to read articles from the site homepage.